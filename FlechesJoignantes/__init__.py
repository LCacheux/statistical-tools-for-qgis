# -*- coding: utf-8 -*-
"""
/***************************************************************************
 FlechesJoignantes
                                 A QGIS plugin
 Représentation de flux par des flèches joignantes
                             -------------------
        begin                : 2014-10-03
        copyright            : (C) 2014 by Lionel Cacheux
        email                : lionel.cacheux@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

def classFactory(iface):
    # load FlechesJoignantes class from file FlechesJoignantes
    from flechesjoignantes import FlechesJoignantes
    return FlechesJoignantes(iface)
