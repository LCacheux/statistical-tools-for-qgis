# -*- coding: utf-8 -*-
"""
/***************************************************************************
 FlechesJoignantes
                                 A QGIS plugin
 Représentation de flux par des flèches joignantes
                              -------------------
        begin                : 2014-10-03
        copyright            : (C) 2014 by Lionel Cacheux
        email                : lionel.cacheux@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4 import QtCore, QtGui

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from flechesjoignantesdialog import FlechesJoignantesDialog
import os.path


import math

# Import the PyQt and QGIS libraries
from PyQt4 import QtGui
from qgis.utils import *
# Pour afficher un message dans la barre de message
from PyQt4.QtGui import QProgressBar
from qgis.gui import QgsMessageBar
from qgis.utils import iface
# Import the utilities from the fTools plugin (a standard QGIS plugin),
# which provide convenience functions for handling QGIS vector layers
import sys, os, imp
import fTools

class FlechesJoignantes:


    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n', 'flechesjoignantes_{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

	path = os.path.dirname(fTools.__file__)
	self.ftu = imp.load_source('ftools_utils', os.path.join(path,'tools','ftools_utils.py'))

        # Create the dialog (after translation) and keep reference
        self.dlg = FlechesJoignantesDialog()

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/flechesjoignantes/iconFlechesJoignantes.png"),
            u"Flèches joignantes", self.iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)

        # Add toolbar button and menu item
        #self.iface.addToolBarIcon(self.action)
        #self.iface.addPluginToMenu(u"&Flèches joignantes", self.action)

        # Add toolbar button and menu item
        if hasattr( self.iface, 'addDatabaseToolBarIcon' ):
            self.iface.addVectorToolBarIcon(self.action)
        else:
            self.iface.addToolBarIcon(self.action)
        if hasattr( self.iface, 'addPluginToVectorMenu' ):
            self.iface.addPluginToVectorMenu( u"Outils statistiques", self.action )
        else:
            self.iface.addPluginToMenu("Outils statistiques", self.action)

    def unload(self):
        # Remove the plugin menu item and icon
        #self.iface.removePluginMenu(u"&Flèches joignantes", self.action)
        #self.iface.removeToolBarIcon(self.action)

        if hasattr( self.iface, 'removePluginVectorMenu' ):
            self.iface.removePluginVectorMenu( u"Outils statistiques", self.action )
        else:
            self.iface.removePluginMenu( u"Outils statistiques", self.action )
        if hasattr( self.iface, 'removeVectorToolBarIcon' ):
            self.iface.removeVectorToolBarIcon(self.action)
        else:
            self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        # Populate the combo boxes
        self.dlg.populateLayers()
        self.dlg.populateTables()
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result == 1:
            # do something useful (delete the line containing pass and
            # substitute with your code)
	    # récupération des paramètres		    
            QApplication.setOverrideCursor( QCursor( Qt.WaitCursor ) )  # souris traitement en cours


            couche = self.ftu.getMapLayerByName(self.dlg.fondDeCarte.currentText())  # couche pour extraction des coordonnées

            nomVarIdGeographique = self.dlg.idGeographique.currentText()
            rangIdentifiant = couche.fieldNameIndex(nomVarIdGeographique)
            flux = self.ftu.getMapLayerByName(self.dlg.tableFlux.currentText())  # table des flux
            nomVarOrigine = self.dlg.varOrigine.currentText()
            rangVarOrigine = flux.fieldNameIndex(nomVarOrigine)
            nomVarDestination = self.dlg.varDestination.currentText()
            rangVarDestination = flux.fieldNameIndex(nomVarDestination)
            nomVarFlux = self.dlg.varFlux.currentText()
            rangVarAnalyse = flux.fieldNameIndex(nomVarFlux)
            Flux_minimum = self.dlg.fluxMin.value()
            Distance_maximale_en_km = self.dlg.distMax.value()

	    # emprise de la couche vecteur
	    xMax = couche.extent().xMaximum()
            yMax = couche.extent().yMaximum()
	    xMin = couche.extent().xMinimum()
	    yMin = couche.extent().yMinimum()
	    xEtendue = xMax-xMin
	    yEtendue = yMax - yMin
	    if xEtendue < yEtendue:
	        etendueMin = xEtendue
	    else:
	        etendueMin = yEtendue

            coordonnees = self.dicoCoordonnees(couche,rangIdentifiant)
            tableFlux,fluxMax, horsChamp = self.listeDesFlux(flux, rangVarOrigine, rangVarDestination, rangVarAnalyse, coordonnees, Flux_minimum, Distance_maximale_en_km)

            if len(tableFlux)>0 :
                if self.dlg.echellePerso.isChecked() and self.dlg.fluxImpose.value() > 0 :
                    fluxMax = self.dlg.fluxImpose.value()

                #Creation d'une nouvelle couche de fleches
                variablesAttributaires = [(QgsField("ORIGINE", QVariant.String)) , (QgsField("DEST", QVariant.String)), (QgsField("FLUX", QVariant.Double)), (QgsField("DISTANCE", QVariant.Double))]
                # outputLayer = VectorWriter(Fleches_joignantes, None, variablesAttributaires, QGis.WKBPolygon, couche.crs())
                crsString = couche.crs().authid()  # scr de la couche d'origine
    	        outputLayer = QgsVectorLayer("Polygon?crs=" + crsString, "Fleches_joignantes_"+flux.name(), "memory")
                outputLayer.startEditing()
	        outputLayer.dataProvider().addAttributes(variablesAttributaires)
	        outputLayer.updateFields()
      
                if self.dlg.sortieShapefile.isChecked():
		    shapefilename = self.dlg.fichierAnalyse.text()
                    legendeShapefileName = self.dlg.fichierLegende.text()            

            
                largeurMax = self.largeurMaxFleche(self.dlg.largeurImposee.value(), etendueMin)
                debordementFleche = largeurMax *.07
                listeFleches = []
                for elem in tableFlux:
                    largeur = elem[0] * largeurMax / fluxMax
                    #print "largeur ="+ str(largeur)+ "   elem =" + str(elem[4])
                    longueurPointe = self.longueurPointeFleche(elem[4], largeur)
                    #print "longueurPointe = " + str(longueurPointe)
                    dessin = self.fleche(elem[4], largeur, longueurPointe, debordementFleche)
                    #print dessin
                    dessin = self.rotationFleche(dessin, elem[5])
                    #print dessin
                    dessin = self.translationFleche(dessin, elem[3])
                    #print dessin
                    listePoints = []
                    for i in range(len(dessin)):
                        #print dessin[i]
	                listePoints.append(QgsPoint(dessin[i][0], dessin[i][1]))
                    outFeat = QgsFeature()
                    outFeat.setGeometry(QgsGeometry.fromPolygon([listePoints]))
                    outFeat.setAttributes([elem[1],elem[2],elem[0],elem[4]])
                    listeFleches.append(outFeat)
                    del outFeat
                outputLayer.addFeatures(listeFleches)

	        outputLayer.commitChanges()
                outputLayer.setSelectedFeatures([])

	        if self.dlg.sortieMemoire.isChecked():       	# chargement de la couche mémoire dans le canevas
	            rendererV2 = outputLayer.rendererV2()
	            # récupère le style des flèches
	            style_path = os.path.join( os.path.dirname(__file__), "flechesJoignantes.qml" )
	            (errorMsg, result) = outputLayer.loadNamedStyle( style_path )
	            QgsMapLayerRegistry.instance().addMapLayer(outputLayer)


	        elif self.dlg.sortieShapefile.isChecked():	# enregistrement du shapefile sur le disque

	            error = QgsVectorFileWriter.writeAsVectorFormat(outputLayer, shapefilename, "CP1250", None, "ESRI Shapefile")
	            if self.dlg.ajoutCanevas.isChecked():	# charge le fond + style
	                layername = os.path.splitext(os.path.basename(str(shapefilename)))[0]
	                outputLayer = QgsVectorLayer(shapefilename, layername, "ogr")
	                rendererV2 = outputLayer.rendererV2()
	                # récupère le style de la légende
	                style_path = os.path.join( os.path.dirname(__file__), "flechesJoignantes.qml" )
	                (errorMsg, result) = outputLayer.loadNamedStyle( style_path )
	                QgsMapLayerRegistry.instance().addMapLayer(outputLayer)





                #Creation d'une nouvelle couche la légende
                variablesAttributaires = [(QgsField("FLUX", QVariant.Double)), (QgsField("LARGEUR", QVariant.Double)), (QgsField("DECALAGE", QVariant.String))]
	        outputLayer = QgsVectorLayer("Polygon?crs=" + crsString, "Fleches_joignantes_Legende_"+flux.name(), "memory")
	        outputLayer.startEditing()
	        outputLayer.dataProvider().addAttributes(variablesAttributaires)
	        outputLayer.updateFields()

 


	        xLegende = xMax + largeurMax * 2
	        yLegende = ( yMax + yMin ) / 2
	        longueurLegende = 2 * largeurMax
	        positionLegendeFleche1 = (xLegende, yLegende)
	        positionLegendeFleche2 = (xLegende, yLegende - largeurMax  - largeurMax / 10)

	        #listeFlux.append([flux,pointOrigine, pointDestination, coordonneesDestination, longueurFlux, angleDirecteur])
	        tableFlux = []
	        tableFlux.append([fluxMax / 3, 'A', 'B', positionLegendeFleche2, longueurLegende, math.pi / 2])
	        tableFlux.append([fluxMax, 'A', 'B', positionLegendeFleche1, longueurLegende, math.pi / 2])

	        #print tableFlux
                listeFleches = []
	        for elem in tableFlux:
	            largeur = elem[0] * largeurMax / fluxMax
	            #print "largeur ="+ str(largeur)+ "   elem =" + str(elem[4])
	            longueurPointe = self.longueurPointeFleche(elem[4], largeur)
	            #print "longueurPointe = " + str(longueurPointe)
	            dessin = self.fleche(elem[4], largeur, longueurPointe, debordementFleche)
	            #print dessin
	            dessin = self.rotationFleche(dessin, elem[5])
	            #print dessin
	            dessin = self.translationFleche(dessin, elem[3])
	            #print dessin
	            listePoints = []
	            for i in range(len(dessin)):
	                #print dessin[i]
	    	        listePoints.append(QgsPoint(dessin[i][0], dessin[i][1]))
                    outFeat = QgsFeature()
	            outFeat.setGeometry(QgsGeometry.fromPolygon([listePoints]))
 	            outFeat.setAttributes([elem[0], largeur, str(longueurLegende)+',0'])
                    listeFleches.append(outFeat)
                    del outFeat
 	        outputLayer.addFeatures(listeFleches)
	        outputLayer.commitChanges()
                outputLayer.setSelectedFeatures([])

	        if self.dlg.sortieMemoire.isChecked():       	# chargement de la couche mémoire dans le canevas
	            rendererV2 = outputLayer.rendererV2()
	            # récupère le style des flèches
	            style_path = os.path.join( os.path.dirname(__file__), "legendeFlechesJoignantes.qml" )
	            (errorMsg, result) = outputLayer.loadNamedStyle( style_path )
	            QgsMapLayerRegistry.instance().addMapLayer(outputLayer)


	        elif self.dlg.sortieShapefile.isChecked():	# enregistrement du shapefile sur le disque

	            error = QgsVectorFileWriter.writeAsVectorFormat(outputLayer, legendeShapefileName, "CP1250", None, "ESRI Shapefile")
	            if self.dlg.ajoutCanevas.isChecked():	# charge le fond + style
	                layername = os.path.splitext(os.path.basename(str(legendeShapefileName)))[0]
	                outputLayer = QgsVectorLayer(legendeShapefileName, layername, "ogr")
	                rendererV2 = outputLayer.rendererV2()
	                # récupère le style de la légende
	                style_path = os.path.join( os.path.dirname(__file__), "legendeFlechesJoignantes.qml" )
	                (errorMsg, result) = outputLayer.loadNamedStyle( style_path )
	                QgsMapLayerRegistry.instance().addMapLayer(outputLayer)
                del outputLayer
                
        
                #if len(tableFlux) >0 :
                iface.messageBar().pushMessage(u"Analyse en flèches joignantes terminées ", u"Valeur(s) hors carte : %d ; Flux max : %d ; Largeur de flèche max : %d" %(horsChamp, fluxMax, largeurMax ), level = QgsMessageBar.INFO, duration = 30)
            else:
                iface.messageBar().pushMessage(u"ATTENTION  ", u"Il n'y a aucun flux valide à représenter!" , level = QgsMessageBar.WARNING, duration = 0)
            QApplication.restoreOverrideCursor()  # remettre la souris normale

    def dicoCoordonnees(self, couche,colonne):
        ''' 
            Dictionnaire de coordonnées
        '''
        dico = {}
	# limitation aux entités sélectionnées de la carte en cas de sélection 
	if couche.selectedFeatures():
            features = couche.selectedFeatures()
        else:
	    features = couche.getFeatures()

        for elem in features:
            centre = elem.geometry().centroid().asPoint()
            valeur = elem.attributes()[colonne]
            dico[valeur] = centre
        return dico
    

    def longueurPointeFleche(self, longueur, largeur):
        '''
            definition de la longueur de la tete de la fleche 
            avec un angle fixe si la fleche est assez longueur, 
            proportionnelle a la longueur si non
        '''
        angle = math.pi / 12
        longueurPointe = (largeur/2.0) / math.tan(angle)
        if longueur / longueurPointe < 3.0:
            longueurPointe = longueur / 3.0
        return longueurPointe

    def fleche(self, longueur, largeur, longueurPointe, debordementFleche): 
        '''
            dessin de base d'une flèche horizontale
        '''
        listePoints = []
        listePoints.append((0,0))  # origine = pointe de la flÃ¨che
        listePoints.append((longueurPointe,largeur/2 + debordementFleche))
        listePoints.append((longueurPointe,largeur/2))
        listePoints.append((longueur, largeur/2))
        listePoints.append((longueur, -largeur/2))
        listePoints.append((longueurPointe, -largeur/2))
        listePoints.append((longueurPointe, -largeur/2 - debordementFleche))
        listePoints.append((0,0))
        return listePoints
        
    def rotationFleche(self, dessin, angleRotation):    
        '''
            applique une rotation centrée en (0,0) Ã  la flèche selon la direction du flux à  représenter  
        '''
        angleRotation = 3* math.pi/2 -angleRotation 
        listePoints = []
        for i in range(len(dessin)):
            x = dessin[i][0] * math.cos(angleRotation) - dessin[i][1]*math.sin(angleRotation)
            y = dessin[i][0] * math.sin(angleRotation) + dessin[i][1]*math.cos(angleRotation)
            listePoints.append((x,y))
        return listePoints
        
    def translationFleche(self, dessin, vecteur):     
        '''
            positionne la flèche pointe sur le point de destination du flux
        '''
        listePoints = []    
        for i in range(len(dessin)):
            listePoints.append((dessin[i][0]+vecteur[0], dessin[i][1]+vecteur[1]))
        return listePoints
        
    def listeDesFlux(self, tableFlux, rangOrigine, rangDestination, rangFLux, dicoCoordonnees, filtreFluxMin, filtreDistMax):
        '''
            crée une liste contenant la future table attributaire + les paramètres des flèches à  dessiner (longueur + direction)
        '''
        listeFlux = []
        horsChamp = 0
        longueurMax = 0
        for elem in tableFlux.getFeatures():
            try:
                if elem.attributes()[rangFLux] and elem.attributes()[rangDestination] and elem.attributes()[rangOrigine] and (elem.attributes()[rangDestination] != elem.attributes()[rangOrigine]):
                    
                    flux = float(elem.attributes()[rangFLux])
                    pointOrigine = elem.attributes()[rangOrigine]
                
                    pointDestination = elem.attributes()[rangDestination]
                    if flux > filtreFluxMin:
                        coordonneesOrigine = dicoCoordonnees[pointOrigine]
                        coordonneesDestination = dicoCoordonnees[pointDestination]
                        d = QgsDistanceArea()
                        longueurFlux = d.measureLine(coordonneesOrigine,coordonneesDestination)
                        if filtreDistMax == 0 or (filtreDistMax >0 and longueurFlux < filtreDistMax * 1000) :
                            angleDirecteur = d.bearing(coordonneesOrigine,coordonneesDestination)
                            listeFlux.append([flux,pointOrigine, pointDestination, coordonneesDestination, longueurFlux, angleDirecteur])

            except:
                pass

        listeFlux.sort()
        if len(listeFlux)>0:
            fluxMax = listeFlux[-1][0]
        else:
	    fluxMax = NULL
        return listeFlux, fluxMax, horsChamp
                    
    def largeurMaxFleche(self, largeurImposee,etendueMin):
        '''
            calcul de la largeur maximale des flèches
        '''
        if largeurImposee > 0:
            largeurMax = largeurImposee
        else:
            largeurMax = etendueMin / 30      # largeur maximale proportionnelle à  l'étendue min de la couche vecteur (à  tester)  
        #print largeurMax
        return largeurMax

