# -*- coding: utf-8 -*-
"""
/***************************************************************************
 RondsProportionnels
                                 A QGIS plugin
 Analyse en ronds proportionnels
                              -------------------
        begin                : 2014-07-27
        copyright            : (C) 2014 by Lionel Cacheux
        email                : lionel.cacheux@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from rondsproportionnelsdialog import RondsProportionnelsDialog
import os.path


# Import the utilities from the fTools plugin (a standard QGIS plugin),
# which provide convenience functions for handling QGIS vector layers
import sys, os, imp
import fTools

import math

# Used to print a informations in the message bar of the canvas 
from PyQt4.QtGui import QProgressBar
from qgis.gui import QgsMessageBar
from qgis.utils import iface

class RondsProportionnels:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n', 'rondsproportionnels_{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)
	path = os.path.dirname(fTools.__file__)
	self.ftu = imp.load_source('ftools_utils', os.path.join(path,'tools','ftools_utils.py'))

        # Create the dialog (after translation) and keep reference
        self.dlg = RondsProportionnelsDialog()

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/RondsProportionnels/iconRonds.png"),
            u"Analyse en ronds", self.iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(u"&Analyses en ronds", self.action)

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu(u"&Analyses en ronds", self.action)
        self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        # Populate the combo boxes
        self.dlg.populateLayers()
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result == 1:
            # do something useful (delete the line containing pass and
            # substitute with your code)

            # recuperation des infos de la boite de dialogue

            lineLayer = QgsVectorLayer("Polygon", 'name', "memory") 
	    lineLayer.startEditing()  
	    layerData = lineLayer.dataProvider() 
	    layerData.addAttributes([ QgsField("ID", QVariant.String), QgsField("latStart", QVariant.String), QgsField("lonStart", QVariant.String), QgsField("latEnd", QVariant.String), QgsField("lonEnd", QVariant.String) ])#
	    lineLayer.commitChanges()
	    QgsMapLayerRegistry.instance().addMapLayer(lineLayer)  


























    def nouvelleTable(self, couche, colonne):
        # cree une liste [ valeur , (coordonnees) , [attributs] ] triee selon une valeur decroissante
        table = []
        totalColonne = 0
        valeursManquantes = 0

        for elem in couche.getFeatures():
            centre = elem.geometry().centroid().asPoint()
            valeur = elem.attributes()[colonne]
            if not valeur:
                if valeur !=0: valeursManquantes +=1
            else:
                listeElements = [abs(valeur), valeur, centre, elem.attributes()]
                table += [listeElements]
                totalColonne += abs(valeur)
        table.sort()
        table.reverse()
        valeurAbsolueMax = table[0][0]
        return valeurAbsolueMax, totalColonne, table, valeursManquantes


    # Calcul du la surface totale d'un fond de carte
    def surfCouche(self, NomCouche):
        surface = 0
        for elem in NomCouche.getFeatures():
            surface += elem.geometry().area()
        return surface

    # Liste de points pour dessiner un polygone circulaire
    def traceRond(self, centre, rayon):
        angle = 0
        #    pas = 2 * math.pi / 80
        # Adaptation du pas en fonction du rayon

        '''
		if rayon < 286.5 :       #  12 * 75 /(2 *math.pi)    ==>  polygone de 12 cotes pour les petits ronds
		    pas = 0.523598776   # 2 * math.pi /12
		else:
		    pas = 150.0/rayon    #                           ==> arcs de 75 m de longueur
        '''
        # pas = 0.15

        # Adaptation du pas en fonction du rayon
        if rayon <286.5 :       #  12 * 50 /(2 *math.pi)    ==>  polygone de 12 cotes pour les petits ronds
            pas = 0.523598776   # 2 * math.pi /12
        else:
            pas = 150.0/rayon    #                           ==> arcs de 75 m de longueur
        points = []
        while angle <= 2* math.pi :
            x1 = centre[0] + rayon * math.sin(angle)
            y1 = centre[1] + rayon * math.cos(angle)
            points.append(QgsPoint(x1,y1))
            angle += pas
        return points
            
