# -*- coding: utf-8 -*-
"""
/***************************************************************************
 RondsProportionnels
                                 A QGIS plugin
 Analyse en ronds proportionnels
                              -------------------
        begin                : 2014-07-27
        copyright            : (C) 2014 by Lionel Cacheux
        email                : lionel.cacheux@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

 
modifications

21/09/2014 v0.4
vérifie si les attributs RAYON et VALEUR existent déjà dans la table d'attributs. Si oui, on incrémente (ex: VALEUR_1)
  -> évite la création d'un fond vide en cas de doublon de colonnes.
modification de la légende. 
  -> ronds créés par un buffer + rectangle plat pour représenter une ligne rejoignant l'étiquette de la valeur
ne demande le SCR qu'une fois (pour la couche de rons et la légende) en cas de SCR non reconnu (ex : fond d'analyse au format .TAB)

13/09/2014 v0.3
+ analyse sur entités sélectionnées pouvant être étendue au reste de la couche
+ nouvelle légende :
+    traits horizontaux en direction des étiquettes
+    valeurs personnalisées des ronds de la légende
+    styles automatiques des ronds et de la légende
+    les étiquettes suivent la légende en cas de déplacement
+ gestion de l'absence de couche
+ vérification de l'existence d'un nom de fichier pour export Shapefile

07/09/2014 v0.2
optimisation de la création de la couche des ronds (addFeatures sorti de la boucle for)
ronds calculés à l'aide de la fonction buffer
affichage du rayon et de la valeur max de l'échelle dans la barre de message en plus du
nombre de valeurs manquantes
"""

# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from rondsproportionnelsdialog import RondsProportionnelsDialog
import os.path


# Import the utilities from the fTools plugin (a standard QGIS plugin),
# which provide convenience functions for handling QGIS vector layers
import sys, os, imp
import fTools

import math

# Used to print a informations in the message bar of the canvas 
from PyQt4.QtGui import QProgressBar
from qgis.gui import QgsMessageBar
from qgis.utils import iface

class RondsProportionnels:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n', 'rondsproportionnels_{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)
	path = os.path.dirname(fTools.__file__)
	self.ftu = imp.load_source('ftools_utils', os.path.join(path,'tools','ftools_utils.py'))

        # Create the dialog (after translation) and keep reference
        self.dlg = RondsProportionnelsDialog()
        #QApplication.restoreOverrideCursor()  # remettre la souris normale en cas de plantage antérieur

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/RondsProportionnels/iconRonds.png"),
            u"Analyse en ronds", self.iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu(u"&Analyses en ronds", self.action)

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu(u"&Analyses en ronds", self.action)
        self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        # Populate the combo boxes
        self.dlg.populateLayers()
        #self.dlg.ValeursLegende.clear()
        self.dlg.fichierAnalyse.clear()
        self.dlg.fichierLegende.clear()
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()

        # See if OK was pressed
        if result == 1:

	    # do something useful (delete the line containing pass and
	    # substitute with your code)

	    if self.dlg.variableAnalyse.currentText() == '':
	            iface.messageBar().pushMessage(u"Erreur", u" Pas de données à analyser" , level = QgsMessageBar.WARNING, duration = 10)
            elif (self.dlg.sortieShapefile.isChecked() and self.dlg.fichierAnalyse.text() == '')  :
                    iface.messageBar().pushMessage(u"Erreur shapefile ", u" Nom de fichier absent ou incorrect" , level = QgsMessageBar.WARNING, duration = 10)
            else:

		    QApplication.setOverrideCursor( QCursor( Qt.WaitCursor ) )  # souris traitement en cours

		    # recuperation des infos de la boite de dialogue

		    inputLayer = self.ftu.getMapLayerByName(self.dlg.fondDeCarte.currentText())

		    nomVariableAnalyse = self.dlg.variableAnalyse.currentText()
		    rangVariable = inputLayer.fieldNameIndex(nomVariableAnalyse)
		    valMax, sommeValeurs, table2, valeursManquantes = self.nouvelleTable(inputLayer, rangVariable)

		    # Echelle 
		    if self.dlg.echelleContour.isChecked():		# somme des aires des ronds = 1/7 aire d'un contour
		        contour = self.ftu.getMapLayerByName(self.dlg.contourAnalyse.currentText())
		        echelleImposee = False
		        surfaceContour = self.surfCouche(contour)
		        coefficient = math.sqrt(surfaceContour/(7*math.pi*sommeValeurs))
		        echelleOk = True

		    else:  						# échelle définie par l'utilisateur
		        valeurMaxImposee = self.dlg.valeurMax.value()
		        rayonMaxImpose = self.dlg.rayonMax.value()
		        echelleImposee = True
		        if (valeurMaxImposee == 0) or (rayonMaxImpose == 0):  
		               echelleOk = False
		        else:
		            echelleOk = True

		    if echelleOk:
		        # Liste des variables de la table d'attributs de la couche a analyser
		        fieldListeAnalyse = list(inputLayer.pendingFields())

                        # Ajoute un numéro d'ordre à la variable 'VALEUR' en cas de doublon pour éviter la création d'un fond vide
                        valueLabel, iLabel = 'VALEUR', ''  
			compteur = 0
                        while inputLayer.fieldNameIndex(valueLabel+iLabel) >=0:
				compteur += 1
				iLabel = '_'+str(compteur)
			valueLabel +=iLabel	

                        # Ajoute un numéro d'ordre à la variable 'RAYON' en cas de doublon pour éviter la création d'un fond vide
			radiusLabel, iLabel = 'RAYON' , ''
			compteur = 0
                        while inputLayer.fieldNameIndex(radiusLabel+iLabel) >=0:
				compteur += 1
				iLabel = '_'+str(compteur)
                        radiusLabel += iLabel

		        #Ajout des variables 'RAYON' et 'VALEUR' à la table d'attributs
		        fieldListeAnalyse.extend([QgsField(radiusLabel, QVariant.Double, "numeric", 10, 1),QgsField(valueLabel, QVariant.Double, "numeric", 10, 3)])
		        
		        #Creation d'une couche memoire pour stocker l'analyse en ronds
		        crsString = inputLayer.crs().authid()  # scr de la couche d'origine
		        resultLayer = QgsVectorLayer("Polygon?crs=" + crsString, "Analyse_en_ronds_"+inputLayer.name(), "memory")
		        resultLayer.startEditing()
		        resultLayer.dataProvider().addAttributes(fieldListeAnalyse)
		        resultLayer.updateFields()

		        if self.dlg.sortieShapefile.isChecked():
		            shapefilename = self.dlg.fichierAnalyse.text()
		            legendeShapefileName = self.dlg.fichierLegende.text()
		            #if shapefilename == "":
		            #    return 1

		        if echelleImposee:
		             rayonMax = rayonMaxImpose
		        else:
		             rayonMax = math.sqrt(abs(valMax) ) * coefficient    

		        ft = QgsFeature()
		        outFeat_centroides = QgsFeature()
		        outFeat = QgsFeature()
		        point = QgsFeature()
		        listeRonds = []
		        # precisionBuffer = 20

		        for ft in table2:
		            
		            centre = ft[2]         # coordonnées du rond
		            attrs = ft[3]          # données attributaires de l'entité d'origine
		            valeurAbs = ft[0]      # valeur absolue de la modalité à analysée
		            valeur = ft[1]         # modalité à analyser

		            # Calcul du rayon
		            if valeurAbs !=0.0:    # éliminer les valeurs nulles
		                if echelleImposee:
		                    rayon = rayonMaxImpose * math.sqrt(valeurAbs/valeurMaxImposee)
		                else:
		                    rayon = math.sqrt(valeurAbs ) * coefficient

		                outFeat_centroides.setGeometry(centre) # centre du cercle
		                point = QgsGeometry(outFeat_centroides.geometry())
                                precisionBuffer = 3 + 20.0*rayon /5000
		                rond = point.buffer(rayon,precisionBuffer)  # buffer circulaire autour d'un point
		                outFeat = QgsFeature()
		                outFeat.setGeometry(rond)
		                attrs.extend([rayon, valeur])
		                outFeat.setAttributes(attrs)
		                listeRonds.append(outFeat) # liste contenant tous les ronds de l'analyse 
		                del outFeat
		                
		        resultLayer.addFeatures(listeRonds)
		        resultLayer.updateExtents()
		        resultLayer.commitChanges()
		        resultLayer.setSelectedFeatures([])
		        if self.dlg.sortieMemoire.isChecked():       	# chargement de la couche mémoire dans le canevas
		            rendererV2 = resultLayer.rendererV2()
		            # récupère le style des ronds
		            style_path = os.path.join( os.path.dirname(__file__), "ronds_style.qml" )
		            (errorMsg, result) = resultLayer.loadNamedStyle( style_path )
		            QgsMapLayerRegistry.instance().addMapLayer(resultLayer)


		        elif self.dlg.sortieShapefile.isChecked():	# enregistrement du shapefile sur le disque

		            error = QgsVectorFileWriter.writeAsVectorFormat(resultLayer, shapefilename, "CP1250", None, "ESRI Shapefile")
		            if self.dlg.ajoutCanevas.isChecked():	# charge le fond + style
		                layername = os.path.splitext(os.path.basename(str(shapefilename)))[0]
		                resultLayer = QgsVectorLayer(shapefilename, layername, "ogr")
		                rendererV2 = resultLayer.rendererV2()
		                # récupère le style de la légende
		                style_path = os.path.join( os.path.dirname(__file__), "ronds_style.qml" )
		                (errorMsg, result) = resultLayer.loadNamedStyle( style_path )
		                QgsMapLayerRegistry.instance().addMapLayer(resultLayer)

		        # del resultLayer

		        # Legende
		        
		        # création du fond de la légende + table d'attributs
			# X, Y, ALPHA pour un positionnement X, Y des étiquettes (non utilisé dans le style par défaut)
			# POS_LEG = déplacement X, Y des étiquettes par rapport au centroide. Permet un replacement automatique des étiquettes quand on déplace la légende 
		        fieldsListeLegende = []
		        fieldsListeLegende.extend([QgsField("RAYON", QVariant.Double, "numeric", 10,1) , QgsField("VALEUR", QVariant.Double, "numeric", 10,3), QgsField("X", QVariant.Double), QgsField("Y", QVariant.Double), QgsField("ALPHA", QVariant.Double), QgsField("POS_LEG", QVariant.String)])
		        crsString = resultLayer.crs().authid()  # scr de la couche de ronds
		        outputLegende = QgsVectorLayer("Polygon?crs=" + crsString, "Legende_analyse_"+inputLayer.name(), "memory")

		        outputLegende.startEditing()
			outputLegende.dataProvider().addAttributes(fieldsListeLegende)
		        outputLegende.updateFields()

			listeRondsLegende = []
	 
		        # rayon maximum de la légende
		        if echelleImposee:
		            rayonMax = rayonMaxImpose * math.sqrt(valMax/valeurMaxImposee)
		        else:
		            rayonMax = math.sqrt(valMax) * coefficient

		        # rayons des ronds personnalisés
		        valLegende = self.dlg.ValeursLegende.text()
		        valLegende = valLegende.strip().replace(';',' ')
		        listeValLegende = valLegende.split()

		        if len(listeValLegende) ==0:  # rayons des ronds de la légende automatique
		            errorLegende = False
		            listeValLegende = [valMax/9, valMax/3, valMax]
			else:
		            try:			
				for i in range(len(listeValLegende)):  # rayons des ronds de la légende personnalisée
				    listeValLegende[i] = float(listeValLegende[i])
		                errorLegende = False
		            except:   # si erreur, retour à la légende personnalisée + message d'info dans la barre de message
				listeValLegende = [valMax/9, valMax/3, valMax] 
		                errorLegende = True
		        listeValLegende.sort()

		        listeRayons = []   # liste contenant les rayons des ronds de la légende

		        for i in range(len(listeValLegende)):
		            if echelleImposee:
		                rayon = rayonMaxImpose * math.sqrt(listeValLegende[i]/valeurMaxImposee)
		            else:
		                rayon = math.sqrt(listeValLegende[i] ) * coefficient
		            listeRayons.append(rayon)

			# Position de la légende : au milieu et à droite du fond de carte d'analyse 

		        xLegende = inputLayer.extent().xMaximum()+ max(listeRayons) *1.5 
		        yLegende = ( inputLayer.extent().yMinimum() + inputLayer.extent().yMaximum() ) /2

                        xEtiquette1 = 1.7*rayonMax

		        yCentresRonds =[]
		        for i in range(len(listeRayons)):
			    yCentre = yLegende + listeRayons[i]-max(listeRayons)

                            # tracé du rond en utilisant un tampon
		            outFeat2 = QgsFeature()
		  	    point = QgsFeature()
			    point2 = QgsFeature()
			    rond = QgsFeature()
		            valRond = listeValLegende[i]
		            rayonRond = listeRayons[i]
		            point.setGeometry(QgsGeometry.fromPoint(QgsPoint(xLegende, yCentre)))
			    point2 = QgsGeometry(point.geometry())
                            precisionBuffer = 3 + 20.0*rayonRond /10000
			    rond = point2.buffer(rayonRond,precisionBuffer)
			    outFeat2.setGeometry(rond)
                            yEtiquette1 = -listeRayons[i]
                            decalagePosLegende = str(xEtiquette1) + ','+ str(yEtiquette1)
                            outFeat2.setAttributes([rayonRond, valRond, NULL, NULL,NULL,decalagePosLegende])
			    listeRondsLegende.append(outFeat2)

                            # tracé d'une ligne joignant le sommet du rond à son étiquette
                            outFeatLigne = QgsFeature()
                            listePoints2 = self.traceRectanglePlat([xLegende,yCentre] , rayonRond, rayonMax*1.5)
                            outFeatLigne.setGeometry(QgsGeometry.fromPolygon([listePoints2]))
		            outFeatLigne.setAttributes([rayonRond,NULL,NULL,NULL,NULL,NULL])
                            listeRondsLegende.append(outFeatLigne)
			    
                            del outFeatLigne
			    del outFeat2

		        outputLegende.addFeatures(listeRondsLegende)
		        outputLegende.updateExtents()
		        outputLegende.commitChanges()

		        if self.dlg.sortieMemoire.isChecked():	# ajout du fond légende sur le canevas
			    # customize style
			    rendererV2 = outputLegende.rendererV2()
			    # chargement du style de la légende
			    style_path = os.path.join( os.path.dirname(__file__), "legende_style.qml" )
			    (errorMsg, result) = outputLegende.loadNamedStyle( style_path )
			    QgsMapLayerRegistry.instance().addMapLayer(outputLegende)

		        elif self.dlg.sortieShapefile.isChecked(): # enregistrement de la légende sur disque au format shapefile
		            error = QgsVectorFileWriter.writeAsVectorFormat(outputLegende, legendeShapefileName, "CP1250", None, "ESRI Shapefile")
		            if self.dlg.ajoutCanevas.isChecked():  # chargement de la légende sur le canevas
		                layername = os.path.splitext(os.path.basename(str(legendeShapefileName)))[0]
		                outputLegende = QgsVectorLayer(legendeShapefileName, layername, "ogr")

		        	# # customize style
				rendererV2 = outputLegende.rendererV2()
				# chargement du style de la légende
				style_path = os.path.join( os.path.dirname(__file__), "legende_style.qml" )
				(errorMsg, result) = outputLegende.loadNamedStyle( style_path )
		                QgsMapLayerRegistry.instance().addMapLayer(outputLegende)


		    # message d'information ou d'erreurs
		    if echelleOk == False:     # Erreur sur l'échelle personnalisée
			iface.messageBar().pushMessage(u"Le rayon et la valeur de l'échelle ne peuvent être nulles", "", level = QgsMessageBar.WARNING, duration = 5)

		    else:

			iface.messageBar().pushMessage(u"Analyse en ronds terminée  ", "Valeur(s) manquante(s) : %d ; Valeur max : %d ; Rayon max : %d" %(valeursManquantes, valMax, rayonMax ), level = QgsMessageBar.INFO, duration = 30)
			if errorLegende == True:
			    iface.messageBar().pushMessage(u"Légende automatique générée ", u" Saisie des valeurs personnalisées incorrecte" , level = QgsMessageBar.WARNING, duration = 5)

		    QApplication.restoreOverrideCursor()  # remettre la souris normale



    
    def nouvelleTable(self, couche, colonne):
        '''
            Extrait la table attributaire vers une liste [ valeur , (coordonnees) , [attributs] ] ,
	    La table est triée en fonction de la valeur à analyser pour mettre les petits ronds devant les grands.  
        '''
        table = []
        totalColonne = 0
        valeursManquantes = 0

        valeurAbsolueMax = 0
        totalColonne = 0

	if couche.selectedFeatures():
            features = couche.selectedFeatures()
	else:
	    features = couche.getFeatures()

        for elem in features : 
	    valeur = elem.attributes()[colonne]
	    if valeur:
		totalColonne += abs(valeur)
                if abs(valeur)>valeurAbsolueMax:
                    valeurAbsolueMax = abs(valeur)

        if not couche.selectedFeatures() or self.dlg.selectedFeatures.isChecked():
            features = couche.getFeatures()
	else:
	    features = couche.selectedFeatures()

        for elem2 in features : 
            valeur = elem2.attributes()[colonne]
            if not valeur:
                if valeur !=0: valeursManquantes +=1
            else:
                listeElements = [abs(valeur), valeur, elem2.geometry().centroid() , elem2.attributes()]
                table += [listeElements]

        table.sort()

        return valeurAbsolueMax, totalColonne, table, valeursManquantes


    def surfCouche(self, NomCouche):
        ''' 
           calcul de la surface totale du fond de carte
        '''
        surface = 0
        if NomCouche.selectedFeatures():
	    couche = NomCouche.selectedFeatures()
        else :
	    couche = NomCouche.getFeatures() 
        for elem in couche:
            surface += elem.geometry().area()
        # print 'surface = ' + str(surface) 
        return surface


    def traceRectanglePlat(self, centre, rayon,longueurTrait):
        '''
           Liste de points pour dessiner un rectangle plat pour représenter les lignes de la légende
        '''
        x = centre[0]
	y = centre[1] + rayon
        points = []
        points.append(QgsPoint(x,y))
        points.append(QgsPoint(x+longueurTrait,y))
        points.append(QgsPoint(x+longueurTrait,y))
        points.append(QgsPoint(x,y))
        return points
