# -*- coding: utf-8 -*-
"""
/***************************************************************************
 RondsProportionnelsDialog
                                 A QGIS plugin
 Analyse en ronds proportionnels
                             -------------------
        begin                : 2014-07-27
        copyright            : (C) 2014 by Lionel Cacheux
        email                : lionel.cacheux@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui
import qgis.core as qgis
from ui_rondsproportionnels import Ui_RondsProportionnels

# Import the utilities from the fTools plugin (a standard QGIS plugin),
# which provide convenience functions for handling QGIS vector layers
import sys, os, imp
import fTools
path = os.path.dirname(fTools.__file__)
ftu = imp.load_source('ftools_utils', os.path.join(path,'tools','ftools_utils.py'))


# create the dialog for zoom to point

class RondsProportionnelsDialog(QtGui.QDialog, Ui_RondsProportionnels):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect

        self.setupUi(self)
        self.echelleContour.toggled.connect(self.radio_echelle)
        self.sortieShapefile.toggled.connect(self.radio_shapefile)
	self.buttonBox.rejected.connect(self.reject)
	self.buttonBox.accepted.connect(self.accept)	
        self.fondDeCarte.currentIndexChanged.connect(self.populateAttributes)
        #self.populateLayers()
        #self.fondDeCarte.currentIndexChanged.connect(self.maxValueLayer)
        #self.ValeursLegende.setText('automatique')
        self.oldPath = ''


        self.selectFichierAnalyse.clicked.connect(self.browse)
	# Populate point layer names

	#self.ui.fondDeCarte.addItems([layer.name() for layer in self.plugin.linelayers])

    def radio_echelle(self):
            if self.echelleContour.isChecked():
                    self.contourAnalyse.setEnabled(True)
                    self.label_4.setEnabled(False)
                    self.label_5.setEnabled(False)
                    self.rayonMax.setEnabled(False)
                    #self.rayonMax.value()
                    self.valeurMax.setEnabled(False)
                    #self.valeurMax.clear()
                    #self.selectedFeatures.setEnabled(True)
                    #self.contourSelect.setEnabled(True)

            else:
                    self.contourAnalyse.setEnabled(False)
                    self.label_4.setEnabled(True)
                    self.label_5.setEnabled(True)
                    self.valeurMax.setEnabled(True)
                    self.rayonMax.setEnabled(True)
                    #self.selectedFeatures.setEnabled(False)
                    #self.selectedFeatures.setChecked(False)
                    #self.contourSelect.setEnabled(False)
                    #self.contourSelect.setChecked(False)

    def radio_shapefile(self):
            if self.sortieShapefile.isChecked():
                    self.label_3.setEnabled(True)
                    self.label_8.setEnabled(True)
                    self.ajoutCanevas.setEnabled(True)
                    self.fichierAnalyse.setEnabled(True)
                    self.fichierLegende.setEnabled(True)
                    self.selectFichierAnalyse.setEnabled(True)
                    #self.selectFichierLegende.setEnabled(True)

            else:
                    self.label_3.setEnabled(False)
                    self.label_8.setEnabled(False)
                    self.ajoutCanevas.setEnabled(False)
                    self.fichierAnalyse.setEnabled(False)
                    self.fichierLegende.setEnabled(False)
                    self.fichierAnalyse.clear()
                    self.fichierLegende.clear()

                    self.selectFichierAnalyse.setEnabled(False)
                    #self.selectFichierLegende.setEnabled(False)


    def populateLayers( self ):
	self.fondDeCarte.clear()     #InputLayer
	self.contourAnalyse.clear()  #ExtentLayer
        myListFonds = []
        myListContours = []
        myListFonds = ftu.getLayerNames( [ qgis.QGis.Polygon, qgis.QGis.Point ] )
        myListContours = ftu.getLayerNames( [ qgis.QGis.Polygon ] )
        self.fondDeCarte.addItems( myListFonds )
        self.contourAnalyse.addItems( myListContours )
  

    def populateAttributes( self ):

        layerName = self.fondDeCarte.currentText()
        #self.ValeursLegende.clear()
        self.variableAnalyse.clear()
        if layerName != "":         
            layer = qgis.QgsMapLayerRegistry.instance().mapLayersByName(layerName)[0]
            fieldList = [field.name()
               for field in list(layer.pendingFields().toList())
               if field.type() in (QtCore.QVariant.Double, QtCore.QVariant.Int)]
            self.variableAnalyse.addItems(fieldList)
            #self.maxValueLayer()


    def browse( self ):

        fileName0 = QtGui.QFileDialog.getSaveFileName(self, 'Enregistrer sous',
                                        self.oldPath, "Shapefile (*.shp);;All files (*)")
        fileName = os.path.splitext(str(fileName0))[0]+'.shp'
        if os.path.splitext(str(fileName0))[0] != '':
            self.oldPath = os.path.dirname(fileName)
        legendeFileName = os.path.splitext(str(fileName0))[0]+'_legende.shp'
        layername = os.path.splitext(os.path.basename(str(fileName)))[0]
        legendeLayerName = os.path.splitext(os.path.basename(str(legendeFileName)))[0]
        if (layername=='.shp'):
            return
        self.fichierAnalyse.setText(fileName)
        self.fichierLegende.setText(legendeFileName)

    '''
    def maxValueLayer(self):
        self.ValeursLegende.clear()
        layerName = self.fondDeCarte.currentText()

        if layerName != "":         
            vlayer = qgis.QgsMapLayerRegistry.instance().mapLayersByName(layerName)[0]

            nomVar = self.variableAnalyse.currentText()
            rangVar = vlayer.fieldNameIndex(nomVar)
            if rangVar >=0:
                maximum = self.maxAbs (vlayer, rangVar )
                textLegende = str(maximum)+';'+str(maximum/3)+';'+str(maximum/9)
                #textLegende = "automatique"
                self.ValeursLegende.setText(textLegende)
                print textLegende

    def maxAbs(self,couche, colonne):
        valeurAbsolueMax = 0
        listeVal = []
	if self.selectedFeatures.isChecked() :
	    features = couche.selectedFeatures()
        else:
            features = couche.getFeatures()
        if features:
	    for elem in features : 
	        valeur = elem.attributes()[colonne]
	        if valeur:
                    #if abs(valeur)>valeurAbsolueMax:
                        #valeurAbsolueMax = abs(valeur)
                    listeVal.append(valeur)
            valeurAbsolueMax = max(listeVal)          
        return valeurAbsolueMax

    '''
