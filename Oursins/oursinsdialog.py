# -*- coding: utf-8 -*-
"""
/***************************************************************************
 OursinsDialog
                                 A QGIS plugin
 Analyse en oursins
                             -------------------
        begin                : 2014-10-04
        copyright            : (C) 2014 by Lionel Cacheux
        email                : lionel.cacheux@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui
import qgis.core as qgis
from ui_oursins import Ui_Oursins
# create the dialog for zoom to point

# which provide convenience functions for handling QGIS vector layers
import sys, os, imp
import fTools
path = os.path.dirname(fTools.__file__)
ftu = imp.load_source('ftools_utils', os.path.join(path,'tools','ftools_utils.py'))


class OursinsDialog(QtGui.QDialog, Ui_Oursins):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)


	self.buttonBox.rejected.connect(self.reject)
	self.buttonBox.accepted.connect(self.accept)
        self.oldPath = ''
        self.selectFichierAnalyse.clicked.connect(self.browse)
        self.sortieShapefile.toggled.connect(self.radio_shapefile)
        self.fondDeCarte.currentIndexChanged.connect(self.populateAttributesFondDeCarte)
        self.tableFlux.currentIndexChanged.connect(self.populateAttributesTableFlux)


    def radio_filtrage(self):
            if self.filtrage.isChecked():
                    self.label_17.setEnabled(True)
                    self.label_18.setEnabled(True)

            else:
                    self.label_17.setEnabled(False)
                    self.label_18.setEnabled(False)

    def radio_shapefile(self):
            if self.sortieShapefile.isChecked():
                    #self.label_4.setEnabled(True)
                    #self.label_5.setEnabled(True)
                    self.ajoutCanevas.setEnabled(True)
                    self.fichierAnalyse.setEnabled(True)
                    #self.fichierLegende.setEnabled(True)
                    self.selectFichierAnalyse.setEnabled(True)
                    #self.selectFichierLegende.setEnabled(True)

            else:
                    #self.label_4.setEnabled(False)
                    #self.label_5.setEnabled(False)
                    self.ajoutCanevas.setEnabled(False)
                    self.fichierAnalyse.setEnabled(False)
                    #self.fichierLegende.setEnabled(False)
                    self.fichierAnalyse.clear()
                    #self.fichierLegende.clear()
                    self.selectFichierAnalyse.setEnabled(False)
                    #self.selectFichierLegende.setEnabled(False)



    def browse( self ):

        fileName0 = QtGui.QFileDialog.getSaveFileName(self, 'Enregistrer sous',
                                        self.oldPath, "Shapefile (*.shp);;All files (*)")
        fileName = os.path.splitext(str(fileName0))[0]+'.shp'
        if os.path.splitext(str(fileName0))[0] != '':
            self.oldPath = os.path.dirname(fileName)
        #legendeFileName = os.path.splitext(str(fileName0))[0]+'_legende.shp'
        layername = os.path.splitext(os.path.basename(str(fileName)))[0]
        #legendeLayerName = os.path.splitext(os.path.basename(str(legendeFileName)))[0]
        if (layername=='.shp'):
            return
        self.fichierAnalyse.setText(fileName)
        #self.fichierLegende.setText(legendeFileName)

    def populateLayers( self ):
	self.fondDeCarte.clear()     #InputLayer
        myListFonds = []
        myListFonds = ftu.getLayerNames( [ qgis.QGis.Polygon, qgis.QGis.Point ] )
        self.fondDeCarte.addItems( myListFonds )

    def populateTables( self ):
	self.tableFlux.clear()     #InputTable
        myList = []
        myList = ftu.getLayerNames([qgis.QGis.NoGeometry])
        self.tableFlux.addItems( myList )

    def populateAttributesFondDeCarte( self ):

        layerName = self.fondDeCarte.currentText()
        self.idGeographique.clear()
        if layerName != "":         
            layer = qgis.QgsMapLayerRegistry.instance().mapLayersByName(layerName)[0]
            fieldList = [field.name()
               for field in list(layer.pendingFields().toList())
               if field.type() not in (QtCore.QVariant.Double, QtCore.QVariant.Int)]
            self.idGeographique.addItems(fieldList)

    def populateAttributesTableFlux( self ):

        layerName = self.tableFlux.currentText()
        self.varOrigine.clear()
	self.varDestination.clear()
        if layerName != "":         
            layer = qgis.QgsMapLayerRegistry.instance().mapLayersByName(layerName)[0]
            fieldList = [field.name()
               for field in list(layer.pendingFields().toList())
               if field.type() not in (QtCore.QVariant.Double, QtCore.QVariant.Int)]
            self.varOrigine.addItems(fieldList)
            self.varDestination.addItems(fieldList)
            fieldList2 = [field.name()
               for field in list(layer.pendingFields().toList())
               if field.type() in (QtCore.QVariant.Double, QtCore.QVariant.Int)]
            self.varFlux.addItems(fieldList2)


