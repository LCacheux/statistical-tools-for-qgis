# -*- coding: utf-8 -*-
"""
/***************************************************************************
 Oursins
                                 A QGIS plugin
 Analyse en oursins
                              -------------------
        begin                : 2014-10-04
        copyright            : (C) 2014 by Lionel Cacheux
        email                : lionel.cacheux@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from oursinsdialog import OursinsDialog
import os.path

import math

# Import the PyQt and QGIS libraries
from PyQt4 import QtGui, QtCore
from qgis.utils import *
# Pour afficher un message dans la barre de message
from PyQt4.QtGui import QProgressBar
from qgis.gui import QgsMessageBar
from qgis.utils import iface
# Import the utilities from the fTools plugin (a standard QGIS plugin),
# which provide convenience functions for handling QGIS vector layers
import sys, os, imp
import fTools

class Oursins:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n', 'oursins_{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

	path = os.path.dirname(fTools.__file__)
	self.ftu = imp.load_source('ftools_utils', os.path.join(path,'tools','ftools_utils.py'))

        # Create the dialog (after translation) and keep reference
        self.dlg = OursinsDialog()

    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(
            QIcon(":/plugins/oursins/oursins.png"),
            u"Analyse en oursins", self.iface.mainWindow())
        # connect the action to the run method
        self.action.triggered.connect(self.run)

        # Add toolbar button and menu item
        #self.iface.addToolBarIcon(self.action)
        #self.iface.addPluginToMenu(u"&Analyse en oursins", self.action)

        # Add toolbar button and menu item
        if hasattr( self.iface, 'addDatabaseToolBarIcon' ):
            self.iface.addVectorToolBarIcon(self.action)
        else:
            self.iface.addToolBarIcon(self.action)
        if hasattr( self.iface, 'addPluginToVectorMenu' ):
            self.iface.addPluginToVectorMenu( u"Outils statistiques", self.action )
        else:
            self.iface.addPluginToMenu("Outils statistiques", self.action)

    def unload(self):
        # Remove the plugin menu item and icon
        #self.iface.removePluginMenu(u"&Analyse en oursins", self.action)
        #self.iface.removeToolBarIcon(self.action)

        if hasattr( self.iface, 'removePluginVectorMenu' ):
            self.iface.removePluginVectorMenu( u"Outils statistiques", self.action )
        else:
            self.iface.removePluginMenu( u"Outils statistiques", self.action )
        if hasattr( self.iface, 'removeVectorToolBarIcon' ):
            self.iface.removeVectorToolBarIcon(self.action)
        else:
            self.iface.removeToolBarIcon(self.action)

    # run method that performs all the real work
    def run(self):
        # Populate the combo boxes
        self.dlg.populateLayers()
        self.dlg.populateTables()
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result == 1:
            # do something useful (delete the line containing pass and
            # substitute with your code)
	    QApplication.setOverrideCursor( QCursor( Qt.WaitCursor ) )  # souris traitement en cours

	    # récupération des paramètres
            couche = self.ftu.getMapLayerByName(self.dlg.fondDeCarte.currentText())  # couche pour extraction des coordonnées
	    # limitation aux entités sélectionnées de la carte en cas de sélection 
	    if couche.selectedFeatures():
                features = couche.selectedFeatures()
	    else:
	        features = couche.getFeatures()
            nomVarIdGeographique = self.dlg.idGeographique.currentText()
            rangIdentifiant = couche.fieldNameIndex(nomVarIdGeographique)
            flux = self.ftu.getMapLayerByName(self.dlg.tableFlux.currentText())  # table des flux
            nomVarOrigine = self.dlg.varOrigine.currentText()
            rangVarOrigine = flux.fieldNameIndex(nomVarOrigine)
            nomVarDestination = self.dlg.varDestination.currentText()
            rangVarDestination = flux.fieldNameIndex(nomVarDestination)
            nomVarFlux = self.dlg.varFlux.currentText()
            rangVarAnalyse = flux.fieldNameIndex(nomVarFlux)
            Flux_minimum = self.dlg.fluxMin.value()
            Distance_maximale_en_km = self.dlg.distMax.value()
 
            coordonnees = self.dicoCoordonnees(couche,rangIdentifiant)
            tableFlux, horsChamp = self.listeDesFlux(flux, rangVarOrigine, rangVarDestination, rangVarAnalyse, coordonnees, Flux_minimum, Distance_maximale_en_km)
            
            #print tableFlux

            #Creation d'une nouvelle couche de lignes
            variablesAttributaires = [(QgsField("ORIGINE", QVariant.String)) , (QgsField("DEST", QVariant.String)), (QgsField("FLUX", QVariant.Double)), (QgsField("DIST_KM", QVariant.Double))]
            crsString = couche.crs().authid()  # scr de la couche d'origine
	    outputLayer = QgsVectorLayer("LineString?crs=" + crsString, "Oursins_"+flux.name(), "memory")
	    outputLayer.startEditing()
	    outputLayer.dataProvider().addAttributes(variablesAttributaires)
	    outputLayer.updateFields()
            if self.dlg.sortieShapefile.isChecked():
		shapefilename = self.dlg.fichierAnalyse.text()


            listeLignes = []
            for elem in tableFlux:
                if elem[1] != elem[2]:
                    #listePoints =[]   
                    outFeat = QgsFeature()
                    listePoints = [QgsPoint(elem[3]), QgsPoint(elem[4])]
                    outFeat.setGeometry(QgsGeometry.fromPolyline(listePoints))
                    outFeat.setAttributes([elem[1], elem[2], elem[0],elem[5]])

                    listeLignes.append(outFeat)
                    del outFeat
            outputLayer.addFeatures(listeLignes)
	    outputLayer.commitChanges()
            outputLayer.setSelectedFeatures([])

	    if self.dlg.sortieMemoire.isChecked():       	# chargement de la couche mémoire dans le canevas
	        rendererV2 = outputLayer.rendererV2()
	        # récupère le style des flèches
	        style_path = os.path.join( os.path.dirname(__file__), "styleOursins.qml" )
	        (errorMsg, result) = outputLayer.loadNamedStyle( style_path )
	        QgsMapLayerRegistry.instance().addMapLayer(outputLayer)


	    elif self.dlg.sortieShapefile.isChecked():	# enregistrement du shapefile sur le disque

	        error = QgsVectorFileWriter.writeAsVectorFormat(outputLayer, shapefilename, "CP1250", None, "ESRI Shapefile")
	        if self.dlg.ajoutCanevas.isChecked():	# charge le fond + style
	            layername = os.path.splitext(os.path.basename(str(shapefilename)))[0]
	            outputLayer = QgsVectorLayer(shapefilename, layername, "ogr")
	            rendererV2 = outputLayer.rendererV2()
	            # récupère le style de la légende
	            style_path = os.path.join( os.path.dirname(__file__), "styleOursins.qml" )
	            (errorMsg, result) = outputLayer.loadNamedStyle( style_path )
	            QgsMapLayerRegistry.instance().addMapLayer(outputLayer)
            
            del outputLayer
            if len(tableFlux) >0 :
                iface.messageBar().pushMessage(u"Analyse en oursins terminée  ", "Valeur(s) hors champ : %d" %(horsChamp ), level = QgsMessageBar.INFO, duration = 30)
            else:
                iface.messageBar().pushMessage(u"ATTENTION  ", u"Il n'y a aucun flux valide à représenter!" , level = QgsMessageBar.WARNING, duration = 0)
            QApplication.restoreOverrideCursor()  # remettre la souris normale


    def dicoCoordonnees(self, couche,colonne):
        ''' 
            Dictionnaire de coordonnées
        '''
        dico = {}
	# limitation aux entités sélectionnées de la carte en cas de sélection 
	if couche.selectedFeatures():
            features = couche.selectedFeatures()
        else:
	    features = couche.getFeatures()

        for elem in features:
            centre = elem.geometry().centroid().asPoint()
            valeur = elem.attributes()[colonne]
            dico[valeur] = centre
        return dico

    def listeDesFlux(self, tableFlux, rangOrigine, rangDestination, rangFLux, dicoCoordonnees, filtreFluxMin, filtreDistMax):
        '''
            crée une liste contenant la future table attributaire + les paramètres des flèches à  dessiner (longueur + direction)
        '''
        listeFlux = []
        horsChamp = 0
        longueurMax = 0
        for elem in tableFlux.getFeatures():
            try:
                if elem.attributes()[rangFLux] and elem.attributes()[rangDestination] and elem.attributes()[rangOrigine] and (elem.attributes()[rangDestination] != elem.attributes()[rangOrigine]):
                    
                    flux = float(elem.attributes()[rangFLux])
                    pointOrigine = elem.attributes()[rangOrigine]
                
                    pointDestination = elem.attributes()[rangDestination]
                    if flux > filtreFluxMin:
                        coordonneesOrigine = dicoCoordonnees[pointOrigine]
                        coordonneesDestination = dicoCoordonnees[pointDestination]
                        d = QgsDistanceArea()
                        longueurFlux = d.measureLine(coordonneesOrigine,coordonneesDestination)
                        if filtreDistMax == 0 or (filtreDistMax >0 and longueurFlux < filtreDistMax * 1000) :
                            listeFlux.append([flux,pointOrigine, pointDestination,coordonneesOrigine, coordonneesDestination, longueurFlux])

            except:
                pass

        # listeFlux.sort()
        # fluxMax = listeFlux[-1][0]
        return listeFlux, horsChamp
                    




           
